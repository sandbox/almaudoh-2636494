<?php

/**
 * @file
 * Contains \Drupal\views_serialized_custom\Plugin\views\field\SerializedCustom
 */

namespace Drupal\views_serialized_custom\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\Serialized;
use Drupal\views\ResultRow;

/**
 * A handler to render serialized fields using custom Twig templates.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("serialized_custom")
 */
class SerializedCustom extends Serialized {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['custom_template'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['format']['#options']['custom_template'] = $this->t('Custom twig template');

    $form['custom_template'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Custom template'),
      '#desciption' => $this->t('A custom twig template to display the unserialized field'),
      '#default_value' => $this->options['custom_template'],
      '#states' => array(
        'visible' => array(
          ':input[name="options[format]"]' => array('value' => 'custom_template'),
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($this->options['format'] == 'custom_template') {
      $value = unserialize($this->getValue($values));
      $render_array = [
        '#type' => 'inline_template',
        '#template' => $this->options['custom_template'],
        '#context' => [
          'item' => $value,
        ],
      ];
      return $this->getRenderer()->renderPlain($render_array);
    }
    else {
      return parent::render($values);
    }
  }

}
